

### Cowell TCP Engine


tcp_states, based on RFC793
|||
|-------|---
|**state**|**description**
|tcp_listen|listening for tcp.syn packet
|tcp_syn|received tcp.syn packet
|tcp_synack|sent tcp.synack reply
|tcp_estab|connection established
|tcp_recvfin|received a fin packet
|tcp_ackwait|waiting for final ack
