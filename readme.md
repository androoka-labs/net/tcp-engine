

### Cowell TCP Engine

The Cowell engine consist of the vhdl core to
manage a tcp/ip port over ethernet that will  
provide wirespeed traffic rates.

1. One connection per port, as defined using dport.
2. TCP/IP Buffers set by rbw (Receive Buffer Width) and tbw (Transmit Buffer Width).

