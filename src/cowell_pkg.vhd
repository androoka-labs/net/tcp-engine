-- 
-- Copyright (C) 2022 Greg Pring
--

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.

-- You should have received a copy of the GNU Lesser General Public License
-- along with this program; if not, write to the Free Software Foundation,
-- Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-- Footer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library net;
use net.ethernet.all;

package cowell_pkg is

  type chan_cfg is record
    dport : integer range 1 to 65535;
    rxw : integer range 0 to 16;
    txw: integer range 0 to 16;
  end record;

  type chan_cfgs is array(integer range <>) of chan_cfg;

  subtype free_used is unsigned(7 downto 0);
  type free_used_a is array(integer range <>) of free_used;

  type macip is record
    mac : mac_t;
    ip : ipv4_t;
  end record;


  -- cowell is  an array of engines
  component cowell is
    generic (
      cfg : chan_cfgs
    );
    port (
      -- gmii rx interface
      rxclk     : in std_logic;
      rxd       : in gmii_t;
      rxdv,rxer : in std_logic;
      -- gmii tx interface
      gtxclk    : in std_logic;
      txd       : out gmii_t;
      txen,txer : out std_logic;
      txreq     : out std_logic;
      txgnt     : in std_logic;
      -- data feed
      clk       : in std_logic;
      arst      : in std_logic;
      -- d-> input data feed
      dused : in free_used_a(cfg'range);
      d    : in bytes(cfg'range);
      dv   : in std_logic_vector(cfg'range);
      dreq : in std_logic_vector(cfg'range);
      -- q -> output data feed
      qfree : in free_used_a(cfg'range);
      q    : out bytes(cfg'range);
      qv   : out std_logic_vector(cfg'range)
    );
  end component;

  component cowell_engine is
    generic (
      cfg : chan_cfg
    );
    port (
      mac       : in mac_t  := x"0203040506a0";
      ipaddr    : in ipv4_t := x"0a030102"; -- 10.3.1.2
      -- gmii rx interface
      rxclk     : in std_logic;
      rxd       : in byte;
      rxdv,rxer : in std_logic;
      -- gmii tx interface
      gtxclk    : in std_logic;
      txd       : out byte;
      txen,txer : out std_logic;
      txreq     : out std_logic;
      txgnt     : in std_logic;
      -- data feed
      clk       : in std_logic;
      arst      : in std_logic;
      -- d-> input data feed
      dused : in free_used := (others => '0');
      d    : in byte := (others => 'X');
      dv   : in std_logic := '0';
      dreq : in std_logic := '0';
      -- q -> output data feed
      qfree : in free_used := (others => '0');
      q    : out byte;
      qv   : out std_logic
    );
  end component;

end package;