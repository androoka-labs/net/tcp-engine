-- 
-- Copyright (C) 2022 Greg Pring
--

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.

-- You should have received a copy of the GNU Lesser General Public License
-- along with this program; if not, write to the Free Software Foundation,
-- Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-- Footer



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library net;
use net.ethernet.all;

-- requires vhdl/2008
library std;
use std.textio.all;

package etherSim is

  function max(a:integer;b:integer) return integer;
  function "+" (l:unsigned;r:std_logic_vector) return unsigned;
  function "+" (l:std_logic_vector;r:std_logic_vector) return unsigned;
  function "+" (l:std_logic_vector;r:integer) return unsigned;

  subtype frame is bytes(1 to 1560);

  function ethernet (
    src : mac_t;
    dst : mac_t;
    etype : unsigned(15 downto 0) := x"4500"
  ) return frame;

  function calc_ipv4_checksum (
    eth : frame
  ) return frame;

  function add_ipv4 (
    eth : frame;
    src : ipv4_t;
    dst : ipv4_t;
    proto : unsigned(7 downto 0);
    ident : unsigned(15 downto 0) := x"0000"
  ) return frame;

  function calc_ipv4 (
    a : integer range 0 to 255;
    b : integer range 0 to 255;
    c : integer range 0 to 255;
    d : integer range 0 to 255
  ) return ipv4_t;

  function add_tcp (
    eth : frame;
    sport : u16;
    dport : u16;
    seq :   u32;
    ack :   u32;
    flag : flags;
    ws : u16;
    urg_ptr : u16 := (others => '0')
  ) return frame;

  function add_tcp_options ( 
    eth : frame;
    opts : bytes
  ) return frame;


  function calc_tcp_checksum (
    eth : frame
  ) return frame;

  function calc_crc32 (
    eth : frame
  ) return std_logic_vector;

  function add_crc32 (
    eth : frame
  ) return frame;

  procedure print_frame (
    eth:frame
  );


end;

package body etherSim is


  function ethernet (
    src : mac_t;
    dst : mac_t;
    etype : unsigned(15 downto 0) := x"4500"
  ) return frame is
    variable e : frame := (others => (others => '-'));
  begin
    for i in 0 to 5 loop
      e(6-i)  := dst(i*8+7 downto i*8);
      e(12-i) := src(i*8+7 downto i*8);
    end loop;
    e(13) := std_logic_vector(etype(15 downto 8));
    e(14) := std_logic_vector(etype( 7 downto 0));
    return e;
  end;

  function calc_ipv4_checksum (
    eth : frame
  ) return frame is
    variable e : frame := eth;
    variable ipcs : unsigned(19 downto 0) := (others => '0');
    variable a : unsigned(15 downto 0);
  begin

    -- zero the checksum field
    e(25) := x"00";
    e(26) := x"00";
    
    for i in 0 to 9 loop
      a := (unsigned(e(15+(i*2))) & unsigned(e(16+(i*2))));
      ipcs := ipcs + a;
      -- report "Entity: adding =" & to_hstring(std_logic_vector(a)) & "h" &
      --   " = " & to_hstring(std_logic_vector(ipcs)) & "h";
    end loop;

    ipcs := resize(ipcs(15 downto 0),ipcs'length) + ipcs(19 downto 16);

    ipcs := ipcs xor x"f_ffff";

    -- report "Entity: ~crc = " & to_hstring(std_logic_vector(ipcs(15 downto 0))) & "h";

    e(25) := std_logic_vector(ipcs(15 downto 8));
    e(26) := std_logic_vector(ipcs(7  downto 0));

    return e;
  end; -- calc_ipv4_crc

  function add_ipv4 (
    eth : frame;
    src : ipv4_t;
    dst : ipv4_t;
    proto : unsigned(7 downto 0);
    ident : unsigned(15 downto 0) := x"0000"
  ) return frame is
    variable e : frame := (others => (others => '-'));
  begin
    e(1 to 14) := eth(1 to 14);

    e(15) := x"45";
    e(16) := x"00";
    e(17) := x"00"; -- size 15..8
    e(18) := std_logic_vector(to_unsigned(20,8)); -- size 7..0

    e(19) := std_logic_vector(ident(15 downto 8));
    e(20) := std_logic_vector(ident( 7 downto 0));
    e(21) := x"00"; -- frag (don't fragment)
    e(22) := x"00"; -- frag offset

    e(23) := x"40"; -- ttl
    e(24) := std_logic_vector(proto); -- protocol
    e(25) := (others => 'X');
    e(26) := (others => 'X');

    for i in 0 to 3 loop
      e((3-i)+27) := src(i*8+7 downto i*8);
      e((3-i)+31) := dst(i*8+7 downto i*8);
    end loop;
    return e;
  end; -- add_ipv4_t

  function calc_ipv4 (
    a : integer range 0 to 255;
    b : integer range 0 to 255;
    c : integer range 0 to 255;
    d : integer range 0 to 255
  ) return ipv4_t is
    variable n : ipv4_t;
  begin
    n(31 downto 24) := std_logic_vector(to_unsigned(a,8));
    n(23 downto 16) := std_logic_vector(to_unsigned(b,8));
    n(15 downto  8) := std_logic_vector(to_unsigned(c,8));
    n( 7 downto  0) := std_logic_vector(to_unsigned(d,8));
    return n;
  end; 
  
  function max(
    a : integer;
    b : integer
  ) return integer is
  begin
    if(a > b) then return b;
    else return a; end if;
  end;

  function "+" (l:unsigned;r:std_logic_vector) return unsigned is
  begin
    return l+ resize(unsigned(r),l'length);
  end function;

  function "+" (l:std_logic_vector;r:std_logic_vector) return unsigned is
  begin return unsigned(l)+ resize(unsigned(r),l'length);
  end function;

  function "+" (l:std_logic_vector;r:integer) return unsigned is
  begin return unsigned(l) + to_unsigned(r,l'length);
  end function;
  
  function "+" (l:unsigned;r:integer) return unsigned is
  begin return l + to_unsigned(r,l'length);
  end function;

  function calc_tcp_checksum (
    eth : frame
  ) return frame is
    variable ef : frame := eth;
    variable cs : unsigned(19 downto 0) := (others => '0');

    variable first,last : integer;

  begin
    -- zero the existing checksum
    ef(51) := x"00";
    ef(52) := x"00";



    -- compute the crc of the Pseudo Header
    -- ipv4 src,dst,proto,tcp segment length

    cs := cs + ef(24);  -- protocol
    cs := cs + 
      (unsigned(ef(27)) & unsigned(ef(28))) + 
      (unsigned(ef(29)) & unsigned(ef(30))) + -- ipv4 src address
      (unsigned(ef(31)) & unsigned(ef(32))) + 
      (unsigned(ef(33)) & unsigned(ef(34))) + -- ipv4 dst address
      (unsigned(ef(47)(7 downto 4))&"00"); -- tcp header length

    for j in 16 to 19 loop
      if cs(j)/='0' and cs(j)/='1' then
        report "checksum is invalid" severity failure;
      end if;
    end loop;

    
    while cs(19 downto 16)/="0000" loop
      cs := to_unsigned(0,cs'length) + cs(15 downto 0) + cs(cs'high downto 16);
    end loop;

    first := 15 + to_integer(unsigned(ef(15)(3 downto 0)))*4; -- ip.header length + 15 should be 35
    last  := 15 + to_integer(unsigned(ef(17))&unsigned(ef(18))) - 1; -- length is ipv4.header + tcp.header + tcp.data(if any)

    for i in first to last loop
      if (i mod 1) = 1 then
        cs := cs + (unsigned(ef(i))&x"00"); -- x??00
      else
        cs := cs + (x"00"&unsigned(ef(i))); -- x00??
      end if;

      for j in 16 to 19 loop
        if cs(j)/='0' and cs(j)/='1' then
          report "checksum is invalid" severity failure;
        end if;
      end loop;


      while cs(19 downto 16)/="0000" loop
        -- should only loop once
        cs := to_unsigned(0,cs'length) + cs(15 downto 0) + cs(cs'high downto 16);
      end loop;

    end loop;

    cs(15 downto 0) := cs(15 downto 0) xor x"ffff";

    ef(51) := std_logic_vector(cs(15 downto 8));
    ef(52) := std_logic_vector(cs( 7 downto 0));

    
    return ef;

  end;


  function add_tcp (
    eth : frame;
    sport : u16;
    dport : u16;
    seq :   u32;
    ack :   u32;
    flag : flags;
    ws : u16;
    urg_ptr : u16 := (others => '0')
  ) return frame is
    variable ef : frame := eth;
    variable iplen : u16;
  begin

    -- ip header ends on 34, tcp starts on 35
    ef(35) := std_logic_vector(sport(15 downto 8));
    ef(36) := std_logic_vector(sport( 7 downto 0));
    ef(37) := std_logic_vector(dport(15 downto 8));
    ef(38) := std_logic_vector(dport( 7 downto 0));

    ef(39) := std_logic_vector(seq(31 downto 24));
    ef(40) := std_logic_vector(seq(23 downto 16));
    ef(41) := std_logic_vector(seq(15 downto  8));
    ef(42) := std_logic_vector(seq( 7 downto  0));

    ef(43) := std_logic_vector(ack(31 downto 24));
    ef(44) := std_logic_vector(ack(23 downto 16));
    ef(45) := std_logic_vector(ack(15 downto  8));
    ef(46) := std_logic_vector(ack( 7 downto  0));

    ef(47) := x"50"; -- data offset 5*4 = 20 from 
    ef(48) := (
      5=> flag.urg,
      4=> flag.ack,
      3=> flag.psh,
      2=> flag.rst,
      1=> flag.syn,
      0=> flag.fin, 
      others => '0');
    ef(49) := std_logic_vector(ws(15 downto 8));
    ef(50) := std_logic_vector(ws( 7 downto 0));

    ef(51) := (others => 'X');
    ef(52) := (others => 'X');
    ef(53) := std_logic_vector(urg_ptr(15 downto 8));
    ef(54) := std_logic_vector(urg_ptr( 7 downto 0));

    iplen := (ef(17)&ef(18)) + (ef(15)(3 downto 0)&"00");
    ef(17) := std_logic_vector(iplen(15 downto 8));
    ef(18) := std_logic_vector(iplen( 7 downto 0));
     
    return ef;
  end;
 
  function add_tcp_options ( 
    eth : frame;
    opts : bytes
  ) return frame is
    variable ef : frame := eth;
    variable dw : integer := opts'length;
    variable i : integer := 0;
    variable b : unsigned(3 downto 0) := unsigned(eth(47)(7 downto 4));
  begin

    while (dw > 0) loop
      ef(47)(7 downto 4) := std_logic_vector(unsigned(ef(47)(7 downto 4)) + 1);

      for j in 0 to 3 loop
        if (opts'low+i) > opts'high then
          ef(55+i) := x"00";
        else
          ef(55+i) := opts(opts'low + i);
        end if;
        i := i + 1;
      end loop;
      dw := dw - 4;
    end loop;



    return ef;
  end;


  function frameLen(eth : frame) return integer is
    variable len : integer := 0;
  begin
    for i in eth'low to eth'high loop
      if eth(i)(7)='-' then
        return len;
      end if;
      len := len + 1;
    end loop;
    return len;
  end;

  function reverse(val : std_logic_vector) return std_logic_vector is
    variable r : std_logic_vector(val'range);
    variable a : integer := val'high;
  begin
    for i in val'low to val'high loop
      r(i) := val(a);
      a := a - 1;
    end loop;
    return r;
  end;


  function calc_crc32 (
    eth : frame
  ) return std_logic_vector is
    variable ef : frame := eth;
    variable crc32 : std_logic_vector(31 downto 0) := x"ffffffff";
    variable len : integer := frameLen(eth);
    variable b : byte;
  begin
    if(len>0) then
      for i in eth'low to (eth'low + len) - 1 loop
        crc32 := calc_crc32_byte(eth(i),crc32);
      end loop;
    end if;
    crc32 := reverse(std_logic_vector(crc32 xor x"ffffffff"));
    return crc32;
  end;

  function add_crc32 (
    eth : frame
  ) return frame is
    variable ef : frame := eth;
    variable crc32 : std_logic_vector(31 downto 0) := x"ffffffff";
    variable i : integer;
  begin
    -- calculate crc32 on valid bytes
    crc32 := calc_crc32(ef);
    i := frameLen(eth);
    ef(i+1) := crc32( 7 downto  0);
    ef(i+2) := crc32(15 downto  8);
    ef(i+3) := crc32(23 downto 16);
    ef(i+4) := crc32(31 downto 24);
    return ef;
  end;

  procedure print_frame (
    eth:frame
  ) is
    variable s : integer := 0;
    variable c : integer := 0;
    variable b : integer := 0;
  begin
    write(output,"[ethernet frame]" & LF);
    for i in eth'range loop
      if eth(i)(0)='-' then
        write(output, "" & LF);
        return;
      else
        if c=0 then
          write(output,to_hstring(to_unsigned(b,8))& ": ");
        end if;
        write(output,"0x" & to_hstring(unsigned(eth(i)))&" ");

        if c=15 then
          write(output,"" & LF);
          c := 0;
          s := 1;
        else
          c := c + 1;
        end if;
      end if;
      b := b + 1;
    end loop;
    write(output, "" & LF);
  end;



end;