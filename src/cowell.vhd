-- 
-- Copyright (C) 2022 Greg Pring
--

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.

-- You should have received a copy of the GNU Lesser General Public License
-- along with this program; if not, write to the Free Software Foundation,
-- Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-- Footer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library net;
use net.ethernet.all;

library cowell;
use cowell.cowell_pkg.all;

-- wrapper for a number of cowell engines

entity cowell is
  generic (
    cfg : chan_cfgs
  );
  port (
    -- gmii rx interface
    rxclk     : in std_logic;
    rxd       : in byte;
    rxdv,rxer : in std_logic;
    -- gmii tx interface
    gtxclk    : in std_logic;
    txd       : out byte;
    txen,txer : out std_logic;
    -- data feed
    clk       : in std_logic;
    arst      : in std_logic;
    -- d-> input data feed
    dused : in free_used_a(cfg'range);
    d    : in bytes(cfg'range);
    dv   : in std_logic_vector(cfg'range);
    dreq : in std_logic_vector(cfg'range);
    -- q -> output data feed
    qfree : in free_used_a(cfg'range);
    q    : out bytes(cfg'range);
    qv   : out std_logic_vector(cfg'range)
  );
end entity;

architecture behav of cowell is

  signal a_txd : bytes(cfg'range);
  signal a_txen, a_txer : std_logic_vector(cfg'range);
  signal a_txreq : std_logic_vector(cfg'range) := (others => '0');
  signal a_txgnt : std_logic_vector(cfg'range) := (others => '0');
  signal tz : std_logic_vector(cfg'range) := (others => '0');
begin

  eng : for i in cfg'range generate
    core : cowell_engine
      generic map (
        cfg => cfg(i)
      ) port map (
        rxclk => rxclk,
        rxd => rxd,
        rxdv => rxdv,
        rxer => rxer,
        gtxclk => gtxclk,
        txd => a_txd(i),
        txen => a_txen(i),
        txer => a_txer(i),
        txreq => a_txreq(i),
        txgnt => a_txgnt(i),
        clk => clk,
        arst => arst,
        dused => dused(i),
        d => d(i),
        dv => dv(i),
        dreq => dreq(i),
        qfree => qfree(i),
        q => q(i),
        qv => qv(i)
      );
  end generate;

  process(gtxclk)
  begin
    if rising_edge(gtxclk) then
      if a_txgnt = tz then 
        -- idle
        for i in cfg'range loop
          if a_txreq(i)='1' then
            a_txgnt(i) <= '1';
            exit;
          end if;
        end loop;

      elsif (a_txgnt and a_txreq) = tz then
        -- requesting no longer requesting
        a_txgnt <= (others => '0');
      end if;
    end if;
  end process;


end architecture;


