-- 
-- Copyright (C) 2022 Greg Pring
--

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.

-- You should have received a copy of the GNU Lesser General Public License
-- along with this program; if not, write to the Free Software Foundation,
-- Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-- Footer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package ethernet is

  subtype gmii_t is std_logic_vector(7 downto 0);
  subtype xgmii_t is std_logic_vector(7 downto 0);

  subtype byte   is std_logic_vector(7 downto 0);
  subtype mac_t  is std_logic_vector(47 downto 0);
  subtype ipv4_t is std_logic_vector(31 downto 0);

  subtype etype is unsigned(15 downto 0);
  subtype ip_port is unsigned(15 downto 0);

  type bytes is array(integer range <>) of byte;

  subtype u16 is unsigned(15 downto 0);
  subtype u32 is unsigned(31 downto 0);
 

  type flags is record
    syn : std_logic;
    ack : std_logic;
    fin : std_logic;
    psh : std_logic;
    urg : std_logic;
    rst : std_logic;
  end record;

  -- function "+"(l : unsigned; r : std_logic_vector) return unsigned;
  -- function "+"(l : std_logic_vector; r : unsigned) return unsigned;
  -- function "+"(l : std_logic_vector; r : std_logic_vector) return unsigned;

  -- function "+"(l : unsigned; r : std_logic_vector) return std_logic_vector;
  -- function "+"(l : std_logic_vector; r : unsigned) return std_logic_vector;
  -- function "+"(l : std_logic_vector; r : std_logic_vector) return std_logic_vector;

  function "+"(l:std_logic_vector;r:std_logic_vector) return std_logic_vector;
  function "+"(l:unsigned;r:std_logic_vector) return std_logic_vector;
  function "+"(l:std_logic_vector;r:unsigned) return std_logic_vector;


  function calc_crc32_byte (
    data : std_logic_vector(7 downto 0);
    crc : std_logic_vector(31 downto 0)
  ) return std_logic_vector;

  -- function reverse(a : std_logic_vector) return std_logic_vector;
  
end package;

package body ethernet is


  function "+"(l:unsigned;r:std_logic_vector) return std_logic_vector is
    variable ret : std_logic_vector(l'range);
  begin
    ret := std_logic_vector(l + unsigned(r));
    return ret;
  end function;

  function "+"(l:std_logic_vector;r:unsigned) return std_logic_vector is
    variable ret : std_logic_vector(l'range);
  begin
    ret := std_logic_vector(unsigned(l) + r);
    return ret;
  end function;

  function "+"(l:std_logic_vector;r:std_logic_vector) return std_logic_vector is
    variable ret : std_logic_vector(l'range);
  begin
    ret := std_logic_vector(unsigned(l) + unsigned(r));
    return ret;
  end function;

  -- function "+"(l : unsigned; r : std_logic_vector) return unsigned is
  --   variable a : unsigned(l'range);
  -- begin
  --   a := l + unsigned(r);
  --   return a;
  -- end function;

  -- function "+"(l : unsigned; r : std_logic_vector) return unsigned is
  --   variable a : unsigned(l'range);
  -- begin
  --   a := l + unsigned(r);
  --   return a;
  -- end function;

  function calc_crc32_byte (
    data : std_logic_vector(7 downto 0); 
    crc : std_logic_vector(31 downto 0)
  ) return std_logic_vector is
    alias d is data;
    alias c is crc;
    -- variable d : std_logic_vector(7 downto 0);
    -- variable c : std_logic_vector(31 downto 0);
    variable next_crc: std_logic_vector(31 downto 0);
  begin
    -- d := data;
    -- c := crc;
    next_crc(0) := d(1) xor d(7) xor c(30) xor c(24);
    next_crc(1) := d(0) xor d(1) xor d(6) xor d(7) xor c(31) xor c(30) xor c(25) xor c(24);
    next_crc(2) := d(0) xor d(1) xor d(5) xor d(6) xor d(7) xor c(31) xor c(30) xor c(26) xor c(25) xor c(24);
    next_crc(3) := d(0) xor d(4) xor d(5) xor d(6) xor c(31) xor c(27) xor c(26) xor c(25);
    next_crc(4) := d(1) xor d(3) xor d(4) xor d(5) xor d(7) xor c(30) xor c(28) xor c(27) xor c(26) xor c(24);
    next_crc(5) := d(0) xor d(1) xor d(2) xor d(3) xor d(4) xor d(6) xor d(7) xor c(31) xor c(30) xor c(29) xor c(28) xor c(27) xor c(25) xor c(24);
    next_crc(6) := d(0) xor d(1) xor d(2) xor d(3) xor d(5) xor d(6) xor c(31) xor c(30) xor c(29) xor c(28) xor c(26) xor c(25);
    next_crc(7) := d(0) xor d(2) xor d(4) xor d(5) xor d(7) xor c(31) xor c(29) xor c(27) xor c(26) xor c(24);
    next_crc(8) := d(3) xor d(4) xor d(6) xor d(7) xor c(28) xor c(27) xor c(25) xor c(24) xor c(0);
    next_crc(9) := d(2) xor d(3) xor d(5) xor d(6) xor c(29) xor c(28) xor c(26) xor c(25) xor c(1);
    next_crc(10) := d(2) xor d(4) xor d(5) xor d(7) xor c(2) xor c(29) xor c(27) xor c(26) xor c(24);
    next_crc(11) := d(3) xor d(4) xor d(6) xor d(7) xor c(3) xor c(28) xor c(27) xor c(25) xor c(24);
    next_crc(12) := d(1) xor d(2) xor d(3) xor d(5) xor d(6) xor d(7) xor c(4) xor c(30) xor c(29) xor c(28) xor c(26) xor c(25) xor c(24);
    next_crc(13) := d(0) xor d(1) xor d(2) xor d(4) xor d(5) xor d(6) xor c(5) xor c(31) xor c(30) xor c(29) xor c(27) xor c(26) xor c(25);
    next_crc(14) := d(0) xor d(1) xor d(3) xor d(4) xor d(5) xor c(6) xor c(31) xor c(30) xor c(28) xor c(27) xor c(26);
    next_crc(15) := d(0) xor d(2) xor d(3) xor d(4) xor c(7) xor c(31) xor c(29) xor c(28) xor c(27);
    next_crc(16) := d(2) xor d(3) xor d(7) xor c(8) xor c(29) xor c(28) xor c(24);
    next_crc(17) := d(1) xor d(2) xor d(6) xor c(9) xor c(30) xor c(29) xor c(25);
    next_crc(18) := d(0) xor d(1) xor d(5) xor c(31) xor c(30) xor c(26) xor c(10);
    next_crc(19) := d(0) xor d(4) xor c(31) xor c(27) xor c(11);
    next_crc(20) := d(3) xor c(28) xor c(12);
    next_crc(21) := d(2) xor c(29) xor c(13);
    next_crc(22) := d(7) xor c(24) xor c(14);
    next_crc(23) := d(1) xor d(6) xor d(7) xor c(30) xor c(25) xor c(24) xor c(15);
    next_crc(24) := d(0) xor d(5) xor d(6) xor c(31) xor c(26) xor c(25) xor c(16);
    next_crc(25) := d(4) xor d(5) xor c(27) xor c(26) xor c(17);
    next_crc(26) := d(1) xor d(3) xor d(4) xor d(7) xor c(30) xor c(28) xor c(27) xor c(24) xor c(18);
    next_crc(27) := d(0) xor d(2) xor d(3) xor d(6) xor c(31) xor c(29) xor c(28) xor c(25) xor c(19);
    next_crc(28) := d(1) xor d(2) xor d(5) xor c(30) xor c(29) xor c(26) xor c(20);
    next_crc(29) := d(0) xor d(1) xor d(4) xor c(31) xor c(30) xor c(27) xor c(21);
    next_crc(30) := d(0) xor d(3) xor c(31) xor c(28) xor c(22);
    next_crc(31) := d(2) xor c(29) xor c(23);
    return next_crc;
  end;

end;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library net;
use net.ethernet.all;

entity ethernet_fcs is
  port (
    clk : in std_logic;
    clr : in std_logic;
    data : in byte;
    valid : in std_logic;
    fcs : out u32
  );
end;

architecture behav of ethernet_fcs is
begin

  process(clk)
    variable crc32 : std_logic_vector(31 downto 0);
  begin
    if rising_edge(clk) then
      if clr='1' then
        crc32 := x"ffffffff";
      end if;
    
      if (valid='1') then

      end if;
    end if;
  end process;

end architecture;
