-- 
-- Copyright (C) 2022 Greg Pring
--

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.

-- You should have received a copy of the GNU Lesser General Public License
-- along with this program; if not, write to the Free Software Foundation,
-- Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-- Footer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library net;
use net.ethernet.all;
use net.etherSim.all;

library cowell;
use cowell.cowell_pkg.all;

entity cowell_engine is
  generic (
    mss : integer := 1460;
    cfg : chan_cfg
  );
  port (
    mac       : in mac_t;
    ipaddr    : in ipv4_t;
    -- gmii rx interface
    rxclk     : in std_logic;
    rxd       : in byte;
    rxdv,rxer : in std_logic;
    -- gmii tx interface
    gtxclk    : in std_logic;
    txd       : out byte;
    txen,txer : out std_logic;
    txreq     : out std_logic;
    txgnt     : in std_logic;
    -- data feed
    clk       : in std_logic;
    arst      : in std_logic;
    -- d-> input data feed
    dused : in free_used := (others => '0');
    d    : in byte := (others => 'X');
    dv   : in std_logic := '0';
    dreq : in std_logic := '0';
    -- q -> output data feed
    qfree : in free_used := (others => '0');
    q    : out byte;
    qv   : out std_logic
  );
end;

architecture behav of cowell_engine is

  -- tcp states rfc793
  type tcp_states is (
    tcp_listen,   -- listening for tcp.syn packet
    tcp_syn,      -- received tcp.syn packet
    tcp_synack,   -- sent tcp.synack packet
    tcp_estab,    -- recv tcp.ack
    tcp_finwait1, -- fin received
    tcp_finwait2,
    tcp_closewait,
    tcp_closing,  --
    tcp_lastack   -- 
  );
  
  signal tcp_state : tcp_states := tcp_listen;

  signal src : macip;

  signal ident : unsigned(15 downto 0);
  signal iplen : unsigned(13 downto 0); -- max 16k

  type sigs is record
    seq : unsigned(31 downto 0);
    ack : unsigned(31 downto 0);
  end record;
  signal rx,tx : sigs;

  signal rxcnt_a : integer range 0 to 8191;

  type states is (ether,ip_hdr,tcp_hdr,tcp_data,nop);
  signal state : states := nop;

begin

  rx_process : process(rxclk)
    variable cnt : integer range 0 to 127;
    variable rxd_p : std_logic_vector(31 downto 0);
  begin
    if rising_edge(rxclk) then
      if rxdv='1' then

        -- a 32bit rolling field allowing
        -- single cycle comparisons
        rxd_p := rxd_p(23 downto 0) & rxd;

        case state is
          when ether =>
            case cnt is
              when 0 =>
              report "processing ethernet header" severity note;
              when 2 =>
                if rxd_p(23 downto 0) /= mac(47 downto 24) then 
                  state <= nop;
                end if;
              when 5 =>
                if rxd_p(23 downto 0) /= mac(23 downto 0) then 
                  state <= nop;
                end if;
              when 8 =>
                if (tcp_state /= tcp_listen) and (rxd_p(23 downto 0) /= src.mac(47 downto 24)) then
                  state <= nop;
                end if;
              when 11 =>
                if (tcp_state /= tcp_listen) and (rxd_p(23 downto 0) /= src.mac(23 downto 0)) then
                  state <= nop;
                end if;
              when 13 => 
                if rxd_p(15 downto 0) /= x"0800" then
                  state <= nop;
                end if;
              when others =>
            end case;

            if (tcp_state = tcp_listen) then
              case cnt is
                when  8 => src.mac(47 downto 24) <= rxd_p(23 downto 0);
                when 11 => src.mac(23 downto 0)  <= rxd_p(23 downto 0);
                when others =>
              end case;
            end if;

            if cnt=13 then
              cnt := 0;
              state <= ip_hdr;
              report "processing ip header" severity note;
            else
              cnt := cnt + 1;
            end if;

          when ip_hdr =>

            case cnt is
              when 0 =>
                if rxd_p(7 downto 0) /= x"45" then
                  state <= nop;
                end if;
              when 3 =>
                if unsigned(rxd_p(15 downto 0)) > mss then 
                  state <= nop;
                end if;
                iplen <= unsigned(rxd_p(iplen'range));
              when 5 =>
                ident <= unsigned(rxd_p(15 downto 0));
              when 7 => -- frag
              when 8 => -- ttl
              when 9 => -- proto
                if rxd_p(7 downto 0) /= x"06" then
                  state <= nop;
                end if;
              when 15 =>
                if tcp_state = tcp_listen then
                  src.ip <= rxd_p(31 downto 0);
                elsif src.ip /= rxd_p(31 downto 0) then

                end if;
              when 19 =>
                if rxd_p /= ipaddr then
                  state <= nop;
                end if;
              when others =>
            end case;


            if cnt=19 then
              cnt := 0;
              state <= tcp_hdr;
              report "processing tcp header" severity note;
            else
              cnt := cnt + 1;
            end if;
          when tcp_hdr =>
            if cnt=19 then
              cnt := 0;
              state <= tcp_data;
            else
              cnt := cnt + 1;
            end if;
          when tcp_data =>
          when nop =>

        end case;

      else
        cnt := 0;
        state <= ether;
      end if;
    end if;
  end process;

end;
