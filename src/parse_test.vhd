
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all; -- Imports the standard textio package.

entity ramtest is
end;


architecture rtl of ramtest is


  signal clk,arstn : std_logic;


  subtype row_t is std_logic_vector(71 downto 0); -- data and enables
  subtype col_t is unsigned(7 downto 0); -- 256 x 72 array
  type mem_t is array(integer range <>) of row_t;


  signal rdaddr : col_t := (others => '0');
  signal q : row_t := (others => '0');
  signal mem : mem_t(0 to (2**col_t'length)-1) := (
    others => (others => '0')
  );

  function to_wide(a : std_logic_vector(63 downto 0)) return std_logic_vector is
    variable r : std_logic_vector(71 downto 0);
  begin
    for i in 0 to 7 loop
      r(i*9+8 downto i*9) := "1" & a(i*8+7 downto i*8);
    end loop;
    return r;
  end;

  type runstates is (rsidle,rsl1,rsl2,rsl3,run);

  signal runstate : runstates := rsidle;

  function shift_w (a : std_logic_vector; b : integer) return std_logic_vector is
    variable r : std_logic_vector(a'range) := a;
  begin
    for i in 1 to b loop
      r(r'high-9 downto 0) := r(a'high downto 9);
      r(r'high downto r'high-8) := (others => 'X');
    end loop;
    return r;
  end function;

  type buf_t is record
    reg : std_logic_vector(71 downto 0);
    ready : boolean;
  end record;
  type buf_a is array(integer range <>) of buf_t;

  signal rd_en : boolean := false;
  signal mr : std_logic_vector(2 downto 0) := "000";

  signal proc_run : boolean := false;
  

  type estates is (idle,ether,ipv4);

  signal preamble_detected : boolean := false;

  signal cy : integer range 0 to 31 := 0;


begin



  process(clk)
    variable ra : col_t := (others => '0');
  begin
    if rising_edge(clk) and rd_en=true then
      q <= mem(to_integer(ra));
      ra := rdaddr;
    end if;
  end process;

  main : process(clk)
    variable srst : std_logic;
    
    -- how many bytes did the cycle parse?
    type strides is (s8,s6,s4,s2,s1,s0); -- 8,4,2,1 bytes at a time
    variable stride : strides := s8;

    -- transfer_byte index. where we last read from buf(0)
    variable transfer_byte_index : integer range 0 to 7 := 0; 

    -- probably 0..2 is enough but needs testing
    variable buf : buf_a(0 to 2);

     -- command line, for processing
    variable cl : std_logic_vector(71 downto 0);

    variable rdstate : integer := 0;

    variable c : integer := 0;

  begin



    if rising_edge(clk) then

      -- case c is
      --   when  0 => stride := s1;
      --   when 31 => stride := s4;
      --   when 32 => stride := s1;
      --   when others =>
      -- end case;

      if proc_run=true then
        -- if buf(0).ready=true and buf(1).ready=true then
        -- process stuff as we have sufficient data

        -- case cl(7 downto 0) is
        --   when x"13" => stride := s4;
        --   when x"27" => stride := s1;
        --   when others =>
        -- end case;

        if cl=x"EAD56AB55AAD56AB55" then
          -- start pre-amble
          preamble_detected <= true;
          stride := s6;
        end if;

        if preamble_detected=true then
          case cy is
            when 0 to 1 => stride := s6;
            when 2 =>   stride := s2; -- ethernet frame type
            when others => stride := s4;
          end case;
          cy <= cy + 1;
        else
          cy <= cy + 1;
        end if;

      end if;


      if true then

        case stride is
          -- cl is the command line, what is used to evaluate
          -- what command length (stride) that cl(7..0) was

          when s0 =>
            -- do noting


          when s1 =>

            -- cl 8..0 needs to be removed

            case transfer_byte_index is
              when 0 => cl := buf(0).reg( 8 downto  0) & cl(71 downto 9);
              when 1 => cl := buf(0).reg(17 downto  9) & cl(71 downto 9);
              when 2 => cl := buf(0).reg(26 downto 18) & cl(71 downto 9);
              when 3 => cl := buf(0).reg(35 downto 27) & cl(71 downto 9);
              when 4 => cl := buf(0).reg(44 downto 36) & cl(71 downto 9);
              when 5 => cl := buf(0).reg(53 downto 45) & cl(71 downto 9);
              when 6 => cl := buf(0).reg(62 downto 54) & cl(71 downto 9);
              when 7 => cl := buf(0).reg(71 downto 63) & cl(71 downto 9);
            end case;

            if transfer_byte_index = 7 then
              transfer_byte_index := 0;
              buf(0).ready := false;
            else
              transfer_byte_index := transfer_byte_index + 1;
            end if;

          when s2 =>

            -- cl 17..0 needs to be removed

            case transfer_byte_index is
              when 0 => cl := buf(0).reg(17 downto  0) & cl(71 downto 18);
              when 1 => cl := buf(0).reg(26 downto  9) & cl(71 downto 18);
              when 2 => cl := buf(0).reg(35 downto 18) & cl(71 downto 18);
              when 3 => cl := buf(0).reg(44 downto 27) & cl(71 downto 18);
              when 4 => cl := buf(0).reg(53 downto 36) & cl(71 downto 18);
              when 5 => cl := buf(0).reg(62 downto 45) & cl(71 downto 18);
              when 6 => cl := buf(0).reg(71 downto 54) & cl(71 downto 18);
              when 7 => cl := buf(1).reg( 8 downto  0) & buf(0).reg(71 downto 63) & cl(71 downto 18);
            end case;

            if transfer_byte_index = 6 then
              transfer_byte_index := 0;
              buf(0).ready := false;
            elsif transfer_byte_index = 7 then
              transfer_byte_index := 1;
              buf(0).ready := false;
            else
              transfer_byte_index := transfer_byte_index + 2;
            end if;

          when s4 =>
            
            -- cl 35..0 needs to be removed (stride = 4)
              
            -- 0..3 will pull data from buf0
            -- 4..7 will also need some from buf1
            case transfer_byte_index is
              -- keeping top 4, need 4 new bytes
              when 0 => cl := buf(0).reg(35 downto  0) & cl(71 downto 36); -- upper cl + pull 4 bytes from buf0
              when 1 => cl := buf(0).reg(44 downto  9) & cl(71 downto 36); -- we have already used buf(0).byte(0)
              when 2 => cl := buf(0).reg(53 downto 18) & cl(71 downto 36); -- we have already used buf(0).byte(0..1)
              when 3 => cl := buf(0).reg(62 downto 27) & cl(71 downto 36); -- we have already used buf(0).byte(0..2)              
              when 4 => cl := buf(0).reg(71 downto 36) & cl(71 downto 36); -- we have already used buf(0).byte(0..3)
              when 5 => cl := buf(1).reg( 8 downto  0) & buf(0).reg(71 downto 45) & cl(71 downto 36);
              when 6 => cl := buf(1).reg(17 downto  0) & buf(0).reg(71 downto 54) & cl(71 downto 36);
              when 7 => cl := buf(1).reg(26 downto  0) & buf(0).reg(71 downto 63) & cl(71 downto 36);
            end case;

            if transfer_byte_index = 4 then
              transfer_byte_index := 0; -- (4+4)%8
              buf(0).ready := false;
            elsif transfer_byte_index = 5 then
              transfer_byte_index := 1;
              buf(0).ready := false;
            elsif transfer_byte_index = 6 then
              transfer_byte_index := 2;
              buf(0).ready := false;
            elsif transfer_byte_index = 7 then
              transfer_byte_index := 3;
              buf(0).ready := false;
            else
              -- still have some bytes to pull from buf(0)
              transfer_byte_index := transfer_byte_index + 4;
            end if;

          when s6 =>
            
            -- cl 35..0 needs to be removed (stride = 4)
              
            -- 0..3 will pull data from buf0
            -- 4..7 will also need some from buf1
            case transfer_byte_index is
              -- keeping top 2, need 6 new bytes
              when 0 => cl := buf(0).reg(53 downto  0) & cl(71 downto 54); -- pull 6, leave 2
              when 1 => cl := buf(0).reg(62 downto  9) & cl(71 downto 54); -- pull 6, leave 1
              when 2 => cl := buf(0).reg(71 downto 18) & cl(71 downto 54); -- pull 6, none left
              when 3 => cl := buf(1).reg( 8 downto  0) & buf(0).reg(71 downto 27) & cl(71 downto 54); -- pull u5 plus 1 from buf1
              when 4 => cl := buf(1).reg(17 downto  0) & buf(0).reg(71 downto 36) & cl(71 downto 54); -- pull u4 plus 2 from buf1 
              when 5 => cl := buf(1).reg(26 downto  0) & buf(0).reg(71 downto 45) & cl(71 downto 54); -- pull u3 pule 3 from buf1
              when 6 => cl := buf(1).reg(35 downto  0) & buf(0).reg(71 downto 54) & cl(71 downto 54); -- pull u2 plus 2 from buf1
              when 7 => cl := buf(1).reg(44 downto  0) & buf(0).reg(71 downto 63) & cl(71 downto 54); -- pill u1 plus 3 from buf1
            end case;

            case transfer_byte_index is
              when 0 to 1 =>
                transfer_byte_index := transfer_byte_index + 2;
              when 2 =>
                transfer_byte_index := 0;
                buf(0).ready := false;
              when 3 =>
                transfer_byte_index := 1;
                buf(0).ready := false;
              when 4 =>
                transfer_byte_index := 2;
                buf(0).ready := false;
              when 5 =>
                transfer_byte_index := 3;
                buf(0).ready := false;
              when 6 =>
                transfer_byte_index := 4;
                buf(0).ready := false;
              when 7 =>
                transfer_byte_index := 5;
                buf(0).ready := false;
            end case;
            
          when s8 =>
          
            case transfer_byte_index is
              when 0 => cl := buf(0).reg(71 downto 0); -- just pull a whole word
              when 1 => cl := buf(1).reg( 8 downto 0) & buf(0).reg(71 downto  9); -- already used buf0.byte(0)
              when 2 => cl := buf(1).reg(17 downto 0) & buf(0).reg(71 downto 18); -- already used buf0.byte(0..1)
              when 3 => cl := buf(1).reg(26 downto 0) & buf(0).reg(71 downto 27); -- etc
              when 4 => cl := buf(1).reg(35 downto 0) & buf(0).reg(71 downto 36);
              when 5 => cl := buf(1).reg(44 downto 0) & buf(0).reg(71 downto 45);
              when 6 => cl := buf(1).reg(53 downto 0) & buf(0).reg(71 downto 54);
              when 7 => cl := buf(1).reg(62 downto 0) & buf(0).reg(71 downto 63);
            end case;

            -- we always taking everything in buf(0)
            -- but as we're incremented by 8 the offset
            -- remains the same.
            buf(0).ready := false;

          when others =>
            report "sorry, not yet implemented" severity error;
        end case;
      end if;

      case rdstate is
        when 0 =>
          -- waiting for new frame
          rd_en <= true;
          if q(8)='1' then
            cl := q;
            rdaddr <= rdaddr + 1;
            rdstate := 1;
          end if;

        when 1 to 2 =>
          -- pipeline read
          rdstate := rdstate + 1;
          rdaddr <= rdaddr + 1;
        
        when 3 to 5 =>

          if rdstate=4 then
            proc_run <= true;
          end if;

          if buf(2).ready=false then
            buf(2).reg   := q;
            buf(2).ready := true;
            rdaddr <= rdaddr + 1;
            rd_en <= true;
          else
            rd_en <= false; -- pause
          end if;
          if rdstate < 5 then
            rdstate := rdstate + 1;
          end if;
        when others =>
      end case;


      -- compress valid buf stages to the bottom
      -- ready for the next cycle of reads
      for i in 0 to buf'high-1 loop
        -- test if we can shuffle down
        if buf(i).ready = false and buf(i+1).ready = true then
          buf(i).ready := true;
          buf(i).reg   := buf(i+1).reg;
          buf(i+1).ready := false;
          buf(i+1).reg := (others => 'X');
        end if;
      end loop;
      
      c := c + 1;

      -- only certain registers needs clearing,
      -- the rest will be written as we go.
      if srst = '1' then
        for i in buf'range loop
          buf(i).reg := (others => 'X');
          buf(i).ready := false;
        end loop;
      end if;

      srst := not arstn;
    end if;

  end process;


  tb : process(clk)
    variable c : integer := 0;
  begin
    if rising_edge(clk) then


      case c is
        when 10 =>
          mem(0) <= to_wide(x"d555555555555555");
          mem(1) <= to_wide(x"0706050403020100");
          mem(2) <= to_wide(x"0f0e0d0c0b0a0908");
          mem(3) <= to_wide(x"1716151413121110");
          mem(4) <= to_wide(x"1f1e1d1c1b1a1918");
          mem(5) <= to_wide(x"2726252423222120");
          mem(6) <= to_wide(x"2f2e2d2c2b2a2928");
          mem(7) <= to_wide(x"3736353433323130");
          mem(8) <= to_wide(x"3f3e3d3c3b3a3938");
          mem(9) <= (others => '0');
        when others =>
      end case;
          

      c := c + 1;
    end if;
  end process;




  process
  begin
    arstn <= '0'; wait for 5 ns;
    arstn <= '1'; wait;
  end process;



  process
  begin
    for a in 0 to 2000 loop
      clk <= '0'; wait for 5 ns;
      clk <= '1'; wait for 5 ns;
    end loop;
    wait;
  end process;

end;
