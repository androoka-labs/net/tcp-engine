-- 
-- Copyright (C) 2022 Greg Pring
--

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.

-- You should have received a copy of the GNU Lesser General Public License
-- along with this program; if not, write to the Free Software Foundation,
-- Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-- Footer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library net;
use net.ethernet.all;
use net.etherSim.all;

library cowell;
use cowell.cowell_pkg.all;

entity cowell_engine_tb is
end;

architecture tb01 of cowell_engine_tb is

  signal clk : std_logic;
  signal arst : std_logic;
  signal rxclk,gtxclk : std_logic;
  signal rxd,txd : byte;
  signal rxdv,rxer,txen,txer : std_logic := '0';
  signal txreq : std_logic;
  alias txgnt is txreq;
  signal d,q : byte := (others => 'X');
  signal dv,qv,dreq : std_logic;
  signal dused,qfree : free_used := (others => '0');

  constant smac : mac_t := x"94e6f74be286";
  constant dmac : mac_t := x"00e067059e7f";
  constant src : ipv4_t := calc_ipv4(10,1,1,2);
  constant dst : ipv4_t := calc_ipv4(10,1,1,3);

  type aa is (one,two,three);
  signal a : aa := one;

begin

  dut : cowell_engine
    generic map (
      cfg => (
        dport => 5000,
        rxw  => 12, -- 4k rx buffer
        txw  => 12  -- 4k tx buffer
      )
    ) port map (
      mac => dmac,
      ipaddr => dst,
      rxclk => rxclk,
      rxd => rxd,
      rxdv => rxdv,
      rxer => rxer,
      gtxclk => gtxclk,
      txd => txd,
      txen => txen,
      txer => txer,
      txreq => txreq,
      txgnt => txgnt,
      clk => clk,
      arst => arst,
      dused => dused,
      d => d,
      dv => dv,
      dreq => dreq,
      qfree => qfree,
      q => q,
      qv => qv
    );

  process(rxclk)
    variable c : integer := 0;
    variable j : integer := 1;
    
    variable ef : frame := (others => (others => '-'));
    variable flag : flags := (others => '0');
    variable seq : u32 := x"12345678";
    variable ack : u32 := (others => '0');
    variable opt_syn : bytes(1 to 9) := (
      1 => x"04", -- sack permitted
      2 => x"02", 
      3 => x"02", -- mss = 0x05b5 (1460)
      4 => x"04",
      5 => x"05",
      6 => x"b4", 
      7 => x"03",
      8 => x"03",
      9 => x"07"
    );
    variable crc32 : std_logic_vector(31 downto 0);
  begin
    if rising_edge(rxclk) then
      case c is
        when 1 =>
          ef := (1 => x"01", 2=> x"02", --3=> x"03", 4=> x"04",
            others => (others => '-'));
          ef := add_crc32(ef);
          if(calc_crc32(ef) /= x"2144DF1C") then
            report "crc error" severity failure;
          end if;
          c := c + 1;
  
        when 10 =>
          ef := (others => (others => '-'));
          ef(1 to 60) := (
            x"ff",x"ff",x"ff",x"ff",x"ff",x"ff",x"a2",x"54",x"a8",x"0c",x"ee",x"db",x"08",x"06",x"00",x"01",
            x"08",x"00",x"06",x"04",x"00",x"01",x"a2",x"54",x"a8",x"0c",x"ee",x"db",x"0a",x"01",x"03",x"0a",
            x"00",x"00",x"00",x"00",x"00",x"00",x"0a",x"01",x"03",x"0e",x"00",x"00",x"00",x"00",x"00",x"00",
            x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00"
          );
          ef := add_crc32(ef);
          crc32 := calc_crc32(ef);
          if(calc_crc32(ef) /= x"2144DF1C") then
            report "crc error" severity failure;
          end if;
          c := c + 1;

        when 20 =>
          ef := (others => (others => '-'));
          ef := ethernet(smac,dmac,x"0800");
          ef := add_ipv4(ef,src,dst,x"06",x"40ea");
          flag.syn := '1';
          ef := add_tcp(ef,x"3841",x"2000",seq,ack,flag,x"01ff",x"0000");
          ef := add_tcp_options(ef,opt_syn);
          ef := calc_ipv4_checksum(ef);
          ef := calc_tcp_checksum(ef);
          ef := add_crc32(ef);

          print_frame(ef);
          c := c + 1;
          j := 1;

        when 21 =>
          if ef(j)(0)='-' then
            rxd <= (others => 'X');
            rxdv <= '0';
            rxer <= '0';
            c := c + 1;
          else
            rxd <= ef(j);
            rxdv <= '1';
            rxer <= '0';
            j := j + 1;
          end if;
        when others =>
          rxdv <= '0';
          rxer <= '0';
          c := c + 1;
      end case;

    end if;

  end process;

  process
  begin
    arst <= '1'; wait for 20 ns;
    arst <= '0'; wait;
  end process;

  process
  begin
    write (output, "[start: cowell_engine_tb]" & LF);
    for i in 0 to 200 loop
      rxclk <= '0'; wait for 4 ns;
      rxclk <= '1'; wait for 4 ns;
    end loop;
    write (output, "[end: cowell_engine_tb]" & LF);
    wait;
  end process;

end architecture;